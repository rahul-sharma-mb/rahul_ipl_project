const csv=require("csvtojson")
const fs= require("fs")
const functions= require("./ipl")

const Deleveries_FilePath = "../data/deliveries.csv"
const Matches_FilePath= "../data/matches.csv"
const outputFile1="../public/output/tossAndMatchWinner.json"
const outputFile2="../public/output/highestMOMperSeasn.json"
const outputFile3= "../public/output/highestSRperSeason.json"
const outputFile4= "../public/output/bestEconsSuperOver.json"
const outputFile5= "../public/output/highestDismissals.json"

function main(){
    csv().fromFile(Deleveries_FilePath).then((deleveries)=>{

        csv().fromFile(Matches_FilePath).then((matches)=>{

                let tossAndMatchWinnerRes= functions.tossAndMatchWinner(matches)
               let highestMOMperSeasnRes=functions.highestMOMperSeasn(matches)
                let bestEconsSuperOverRes=functions.bestEconsSuperOver(deleveries)
               let highestSRperSeasonRes=functions.highestSRperSeason(deleveries,matches)
               let highestDismissalsRes=functions.highestDismissals(deleveries)

                writeDataToFile(tossAndMatchWinnerRes,outputFile1)
                writeDataToFile(highestMOMperSeasnRes,outputFile2)
                writeDataToFile(highestSRperSeasonRes,outputFile3)
               writeDataToFile(bestEconsSuperOverRes,outputFile4)
                writeDataToFile(highestDismissalsRes,outputFile5)

        })
    })

    const writeDataToFile =(res,path)=>{
        let objStr= JSON.stringify(res)

    fs.writeFile(path,objStr,"utf-8",(err)=>{
        if(err){
            console.log(err)
        }
    })
    }


}

main()
