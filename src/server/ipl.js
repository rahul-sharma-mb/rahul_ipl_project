//1.bestEconsSuperOver-begin
const bestEconsSuperOver =(deliveries)=>{

    let bowlers= deliveries.filter(item=>{
        return item.is_super_over ==1
     }).reduce((bowlers,ball)=>{
         if(bowlers[ball.bowler]==undefined){
             bowlers[ball.bowler]={}
             bowlers[ball.bowler]["balls"]=1
             bowlers[ball.bowler]["runs"]= +ball.batsman_runs + +ball.noball_runs + +ball.wide_runs
         }else{
             bowlers[ball.bowler]["balls"]+=1
             bowlers[ball.bowler]["runs"]+= +ball.batsman_runs + +ball.noball_runs + +ball.wide_runs
         }
         return bowlers

     },{})


 let econmomyArr=[]
 for(item in bowlers){
     let economy=(bowlers[item]["runs"]/(bowlers[item]["balls"]/6)).toFixed(2)
     econmomyArr.push([item,economy])
 }
econmomyArr= (econmomyArr.sort((a,b)=>a[1]-b[1])).slice(0,10)
return Object.fromEntries(econmomyArr)

   }
//1.bestEconsSuperOver-end



//2 highest dismissals- begin
const highestDismissals =(deleveries)=>{
    let filteredDeleveries= deleveries.filter((del)=>(del.player_dismissed)&&(del.dismissal_kind!="run out"))
    let dismissalObj= (filteredDeleveries).reduce((dismissal,item)=>{
    let players =[`batsman:${item.batsman},bowler:${item.bowler}`]
      dismissal[players]=dismissal[players]+1||1
      return dismissal

    },{})
    // console.log(dismissalObj)
let dismissalsArr= Object.entries(dismissalObj)
dismissalsArr= dismissalsArr.sort((a,b)=>b[1]-a[1])
let dismissalsObj= Object.fromEntries(dismissalsArr.slice(0,10))
return  dismissalsObj
}
 //2 highest dismissals- end


 //3 highest MOMperSeason-end
  const highestMOMperSeasn=(matches)=>{

    let season={}
  for(let year =2008;year<2018;year++){
        season[year]=getHighestMOM(year,matches)
  }
  return season
}
const getHighestMOM=(year,match)=>{


    let filteredMatches=match.filter((item)=>item.season==year).reduce((seasonHighest,match)=>{
         seasonHighest[match.player_of_match]=seasonHighest[match.player_of_match]+1 ||1
         return seasonHighest
    },{})

     let seasonHighestArr= Object.entries(filteredMatches)
    seasonHighestArr=seasonHighestArr.sort(function (a,b)  {return b[1]-a[1]})


return seasonHighestArr[0]

}
 //3 highest MOMperSeason-end




//4.highestSRperSeason-begin

const highestSRperSeason =(deleveries,matches)=>{

    let highestStrikeRate={}
        for(let year =2008;year<2020;year++){
            let filteredSeasonMatchesId= filterMatches(matches,year)

            let filteredSeasonDeleveries=filterDeleveries(filteredSeasonMatchesId,deleveries)

            highestStrikeRate[year]=getTopSRperYear(filteredSeasonDeleveries)

        }

        return highestStrikeRate

}

const filterMatches =(matches,year)=>{
    let filteredMatchIds=matches.filter(item=>item.season==year).reduce((idArr,match)=>{
            idArr.push(match.id)
            return idArr
    },[])
    return filteredMatchIds
}
const filterDeleveries =(id,deleveries)=>{
    return deleveries.filter(item=>id.includes(item.match_id))
}
const getTopSRperYear =(filteredSeasonDeleveries)=>{

    let batsmen= filteredSeasonDeleveries.reduce((batsmen,ball)=>{
         if(batsmen[ball.batsman]==undefined){
             batsmen[ball.batsman]={}
             batsmen[ball.batsman]["balls"]=1
             batsmen[ball.batsman]["runs"]= +ball.batsman_runs
         }else{
             batsmen[ball.batsman]["balls"]+=1
             batsmen[ball.batsman]["runs"]+= +ball.batsman_runs
         }
         return batsmen

     },{})

 let strikeRateArr=[]
 for(item in batsmen){
     let strikeRate=((batsmen[item]["runs"]/(batsmen[item]["balls"]))*100).toFixed(2)

 strikeRateArr.push([item,strikeRate])

 }
  runsArr=strikeRateArr.sort((a,b)=>b[1]-a[1]).slice(0,1)

  return runsArr[0]
}

//4.highestSRperSeason-end


//5. tossAndMatchWinner-begin
const tossAndMatchWinner=(matches)=>{
    return  matches.filter(item=>item.toss_winner==item.winner).reduce((obj,curr)=>{
     obj[curr.toss_winner]=obj[curr.toss_winner]+1||1
     return obj
    },{})
 }


 //5. tossAndMatchWinner-end


module.exports={
    tossAndMatchWinner,
    highestMOMperSeasn,
    bestEconsSuperOver,
    highestSRperSeason,
    highestDismissals,

}
